# Kubernetes Agent Terraform module

Adding official support into the GitLab Terraform provider is tracked in https://gitlab.com/gitlab-org/gitlab/-/issues/227157

Until then, this module uses the GitLab graphql API directly to register a new agent instance, and install it into the cluster using the terraform `kubectl` provider.


## Usage


1. Add providers:
    ```hcl
    terraform {
      required_providers {
        kubernetes = "~> 2.3"
        kubectl = {
          source  = "gavinbunney/kubectl"
          version = "~> 1.0"
        }
        graphql = {
          source = "sullivtr/graphql"
        }
        gitlab = {
          source = "gitlabhq/gitlab"
          version = "~>3.6.0"
        }
      }
    }

    provider "graphql" {
      url = var.gitlab_graphql_api_url
      headers = {
        "Authorization" = "Bearer ${var.gitlab_password}"
      }
    }

    provider "gitlab" {
      token = var.gitlab_password
    }

    provider "kubernetes" {
      alias = "mycluster"
      host = ${YOUR_TF_CLUSTER_ENDPOINT}
      cluster_ca_certificate = ${YOUR_TF_CLUSTER_CA_CERT}
      token = ${YOUR_SERVICE_ACCOUNT_TOKEN}
    }
    ```
2. Add call to this module:
   ```hcl

    module "gitlab_kubernetes_agent" {
      source = "gitlab.com/nagyv-gitlab/kubernetes-agent-terraform-module/local"
      providers = {
        kubernetes = kubernetes.mycluster
      }
      gitlab_project_id = var.gitlab_agent_config_project_id
      gitlab_graphql_api_url = var.gitlab_graphql_api_url

      # REF: # https://gitlab.com/sandlin/gitlab_k8s_cluster/k8s-gitlab-agent-config
      agent_name = "gitlab-agent" 
      agent_namespace = "gitlab-agent" 
      agent_version = "stable" 
      
      token_name = "gitlab-agent"
      token_description = "This token is being generated for the gitlab_agent"
      annotations = {}
    }
   ```

 

## Example main.tf:
```

terraform {
  required_providers {
    kubernetes = "~> 2.3"
    kubectl = {
      source  = "gavinbunney/kubectl"
      version = "~> 1.0"
    }
    graphql = {
      source = "sullivtr/graphql"
    }
    gitlab = {
      source = "gitlabhq/gitlab"
      version = "~>3.6.0"
    }
  }
}

provider "graphql" {
  url = var.gitlab_graphql_api_url
  headers = {
    "Authorization" = "Bearer ${var.gitlab_token}"
  }
}

provider "gitlab" {
    token = var.gitlab_token
}

provider "google" {
  credentials = try(base64decode(file(pathexpand(var.service_account_file))), file(pathexpand(var.service_account_file)))
  project     = var.project
}

# Get the Service Account Token we will use to control Kubernetes.
data "google_service_account_access_token" "sa_token" {
  provider               = google
  target_service_account = google_service_account.sa.email
  scopes                 = ["cloud-platform"]
  lifetime               = "300s"
}

provider "kubernetes" {
  alias = "thiscluster"
  host = "https://${google_container_cluster.primary.endpoint}"
  cluster_ca_certificate = base64decode(google_container_cluster.primary.master_auth.0.cluster_ca_certificate) 
  token = data.google_service_account_access_token.sa_token.access_token
    
}

module "gitlab_kubernetes_agent" {
  source = "gitlab.com/nagyv-gitlab/kubernetes-agent-terraform-module/local"
  providers = {
    kubernetes = kubernetes.thiscluster
  }
  gitlab_project_id = var.gitlab_agent_config_project_id
  gitlab_graphql_api_url = var.gitlab_graphql_api_url

  # REF: https://docs.gitlab.com/ee/user/clusters/agent/install/#install-the-agent-into-the-cluster
  agent_name = "gitlab-agent" 
  agent_namespace = "gitlab-kubernetes-agent" 
  agent_version = "stable" 
  
  token_name = "gitlab-agent"
  token_description = "This token is being generated for the gitlab_agent"
  annotations = {}

  # When using a single TF module to create a cluster & install the agent, You must configure RBAC to allow this module's execution.
  depends_on = [
    kubernetes_cluster_role_binding.terraform_clusteradmin,
  ]
}
```
RBAC configuration in GCP: 
```

# Terraform needs to manage K8S RBAC
# Meanwhile, for GitLab Managed Apps, we need access to endpoints & podsecuritypolicies
# https://cloud.google.com/kubernetes-engine/docs/how-to/role-based-access-control#iam-rolebinding-bootstrap
# This depends upon cluster creation & the id is based upon the example here: https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/container_node_pool


resource "kubernetes_cluster_role" "getNonResourceUrl" {
  provider = kubernetes.thiscluster
  metadata {
    name = "getNonResourceUrl"
    labels = {
      # Add these permissions to the "admin" and "edit" default roles.
      "rbac.authorization.k8s.io/aggregate-to-admin" = "true"
    }
  }

  rule {
    api_groups = [""]
    resources  = ["endpoints", "podsecuritypolicies"]
    verbs      = ["get", "list", "watch", "use"]
  }
}



# Terraform needs to manage K8S RBAC
# https://cloud.google.com/kubernetes-engine/docs/how-to/role-based-access-control#iam-rolebinding-bootstrap
resource "kubernetes_cluster_role_binding" "terraform_clusteradmin" {
  provider = kubernetes.thiscluster
  depends_on = [
    google_container_node_pool.primary_nodes
  ]

  metadata {
    name = "cluster-admin-binding-terraform"
  }

  role_ref {
    api_group = "rbac.authorization.k8s.io"
    kind      = "ClusterRole"
    name      = "cluster-admin"
  }

  subject {
    api_group = "rbac.authorization.k8s.io"
    kind      = "User"
    name      = google_service_account.sa.email
  }

  # must create a binding on unique ID of SA too
  subject {
    api_group = "rbac.authorization.k8s.io"
    kind      = "User"
    name      = google_service_account.sa.unique_id
    namespace = ""
  }
}

```

[Sample Project](https://gitlab.com/sandlin/gke-deploy-env)
---
### Why require the modules here?
> Provider configurations belong in the root module of a Terraform configuration. (Child modules receive their provider configurations from the root module; for more information, see The Module providers Meta-Argument and Module Development: Providers Within Modules.)
- [SOURCE](https://www.terraform.io/language/providers/configuration#provider-configuration-1)
> Terraform v0.11 introduced the mechanisms described in earlier sections to allow passing provider configurations between modules in a structured way, and thus we explicitly recommended against writing a child module with its own provider configuration blocks. However, that legacy pattern continued to work for compatibility purposes -- though with the same drawback -- until Terraform v0.13.

> A module intended to be called by one or more other modules must not contain any provider blocks. A module containing its own provider configurations is not compatible with the for_each, count, and depends_on arguments that were introduced in Terraform v0.13. For more information, see Legacy Shared Modules with Provider Configurations.

> Provider configurations belong in the root module of a Terraform configuration. (Child modules receive their provider configurations from the root module; for more information, see The Module providers Meta-Argument and Module Development: Providers Within Modules.)

- [SOURCE](https://www.terraform.io/language/modules/develop/providers)
